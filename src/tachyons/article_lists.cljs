(ns tachyons.article-lists)

(defn article-list-author-media-flipped [title articles]
  [:section.mw7.center
   (when title [:h2.athelas.ph3.ph0-l title])
   (map-indexed (fn [idx {:keys [title path description date author src alt thumbnail] :as article}]
                  [:article.pv4.bb.b--black-10.ph3.ph0-l
                   {:keys idx}
                   [:div.flex.flex-column.flex-row-ns
                    [:div.w-100.w-60-ns.pr3-ns.order-2.order-1-ns
                     [:a {:href (rfe/href :posts {:path path})}
                      [:h1.f3.athelas.mt0.lh-title title]]
                     [:p.f5.f4-l.lh-copy.athelas description]]
                    (when thumbnail [:div.pl3-ns.order-1.order-2-ns.mb4.mb0-ns.w-100.w-40-ns
                                     [:img.db {:src thumbnail
                                               :alt alt}]])]
                   [:p.f6.lh-copy.gray.mv0 (when author (str "By " author))
                    (when date [:time.f6.db.gray date])]])
                articles)])
