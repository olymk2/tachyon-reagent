(ns tachyons.definition-lists)


(defn simple [{:keys [list]}]
  [:dl.lh-title.pa4.mt0
   (map-indexed (fn [idx {:keys [title text]}]
             [:<> {:key idx}
              [:dt.f6.b title ]
              [:dd.ml0 text]]
   ) list)])
