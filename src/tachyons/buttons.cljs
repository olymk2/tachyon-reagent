(ns tachyons.buttons)

(defn basic [{:keys [href text]}]
  [:a.f6.link.dim.ph3.pv2.mb2.dib.white.bg-black {:href href} text])

(defn centered-icon [{:keys [icon href title text]}]
  [:a.no-underline.near-white.bg-animate.bg-near-black.hover-bg-gray.inline-flex.items-center.ma2.tc.br2.pa2
   {:href href :title title}
   icon
    [:span.f6.ml3.pr2 text]])

(defn pill [{:keys [href text]}]
  [:a.f6.link.dim.br-pill.ph3.pv2.mb2.dib.white.bg-black {:href href} text])

(defn pill-grow [{:keys [href text]}]
  [:a.f6.grow.no-underline.br-pill.ph3.pv2.mb2.dib.white.bg-black {:href href}
   text])
