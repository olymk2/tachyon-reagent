(ns tachyons.headers)

(defn startup-hero [{:keys [title] :as optional}]
  [:header.sans-serif
   [:div.cover.bg-left.bg-center-l
    {:style
     {:background-image (when (:background-image optional)
                          (str "url(" (:background-image optional) ")"))}}
    [:div.bg-black-80.pb5.pb6-m.pb7-l
     [:nav.dt.w-100.mw8.center
      [:div.dtc.w2.v-mid.pa3
       (when (:icon-link optional)
         [:a.dib.w2.h2.pa1.ba.b--white-90.grow-large.border-box
          {:href (:url (:icon-link optional))} title])]
      (when (:links optional)
        [:div.dtc.v-mid.tr.pa3
         (map (fn [{:keys [url title] :as link}]
                [:a.f6.fw4.hover-white.no-underline.white-70.dib-ns.pv2.ph3
                 {:href url
                  :class (:classes link)
                  :on-click (:callback link)} title]) (:links optional))])]
     [:div.tc-l.mt4.mt5-m.mt6-l.ph3
      [:h1.f2.f1-l.fw2.white-90.mb0.lh-title title]
      (when (:sub-title optional)
        [:h2.fw1.f3.white-80.mt3.mb4 (:sub-title optional)])

      (when (:actions optional)
        (map (fn [{:keys [url title] :as link}]
               [:a.f6.no-underline.grow.dib.v-mid.bg-blue.white.ba.b--blue.ph3.pv2.mb3
                {:href url
                 :class (:classes link)
                 :on-click (:callback link)} title])
             (:actions optional)))]]]])
