(ns tachyons.articles)

(defn article-list [{:keys [title articles]}]
  [:section.mw7.center
   [:h2.athelas.ph3.ph0-l title]
   (map (fn [{:keys [title content] :as optional}]
          [:article.bt.bb.b--black-10
           [:a.db.pv4.ph3.ph0-l.no-underline.black.dim
            {:href "#0"}
            [:div.flex.flex-column.flex-row-ns
             (when (:icon optional)
               [:i.fas.f-6.p0.m0.h-100 {:class (:icon optional)}])
             (when (:image optional)
               [:div.pr3-ns.mb4.mb0-ns.w-100.w-40-ns
                [:img.db {:src "http://mrmrs.github.io/photos/cpu.jpg"
                          :alt "Photo of a dimly lit room with a computer interface terminal."}]])
             [:div.w-100.w-60-ns.pl3-ns
              [:h1.f3.fw1.baskerville.mt0.lh-title title]
              [:p.f6.f5-l.lh-copy content]
              (when (:author optional) [:p.f6.lh-copy.gray.mv0 "By " [:span.ttu (:author optional)]])
              (when (:created optional) [:time.f6.db.gray (:created optional)])]]]])

        articles)])

(defn article-list-flipped [{:keys [title articles]}]
  [:section.mw7.center
   [:h2.athelas.ph3.ph0-l title]
   (map (fn [{:keys [title content] :as optional}]
          [:article.pv4.bt.bb.b--black-10.ph3.ph0-l
           [:div.flex.flex-column.flex-row-ns
            [:div.w-100.w-60-ns.pr3-ns.order-2.order-1-ns
             [:h1.f3.athelas.mt0.lh-title title]
             [:p.f5.f4-l.lh-copy.athelas content]]
            (when (:image optional)
              [:div.pl3-ns.order-1.order-2-ns.mb4.mb0-ns.w-100.w-40-ns
               [:img.db {:src "http://mrmrs.github.io/photos/cpu.jpg" :alt "Photo of a dimly lit room with a computer interface terminal."}]])]
           (when (:author optional) [:p.f6.lh-copy.gray.mv0 "By " [:span.ttu (:author optional)]])
           (when (:created optional) [:time.f6.db.gray (:created optional)])]) articles)])


