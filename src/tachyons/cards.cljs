(ns tachyons.cards)

(defn basic-text-card [{:keys [title content]}]
  [:article.center.mw5.mw6-ns.hidden.ba.mv4
   [:h1.f4.bg-near-black.white.mv0.pv2.ph3 title]
   [:div.pa3.bt [:p.f6.f5-ns.lh-copy.measure.mv0 content]]])

(defn text-card [{:keys [title content]}]
  [:article.center.mw5.mw6-ns.br3.hidden.ba.b--black-10.mv4
   [:h1.f4.bg-near-white.br3.br--top.black-60.mv0.pv2.ph3 title]
   [:div.pa3.bt.b--black-10 [:p.f6.f5-ns.lh-copy.measure content]]])

(defn profile-card [{:keys [img-src img-alt name tagline]}]
  [:article.mw5.center.bg-white.br3.pa3.pa4-ns.mv3.ba.b--black-10
   [:div.tc
    [:img.br-100.h4.w4.dib.ba.b--black-05.pa2
     {:src img-src
      :alt img-alt}]
    [:h1.f3.mb2 name]
    [:h2.f5.fw4.gray.mt0 tagline]]])

(defn album-centered [{:keys [link-href link-title img-src img-alt list]}]
  [:a.db.center.mw5.tc.black.link.dim
   {:title link-title
    :href link-href}
   [:img.db.ba.b--black-10
    {:alt img-alt
     :src img-src}]
   (into
    [:dl.mt2.f6.lh-copy]
    (map (fn [{:keys [name value classes]}]
           [:<> 
            [:dt.clip name]
            [:dt.ml0 {:class classes} value]]) list))])
