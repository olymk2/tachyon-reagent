(ns tachyons.navigation)

(defn large-title-link-list [{:keys [title links]}]
  [:nav.pa3.pa4-ns
   [:a.link.dim.black.b.f1.f-headline-ns.tc.db.mb3.mb4-ns
    {:href "#" :title "Home"} title]
   [:div.tc.pb3
    (map (fn [{:keys [title url] :as link}]
           [:a.link.dim.gray.f6.f5-ns.dib.mr3
            {:href url
             :title title
             :on-click (:callback link)} title]) links)]])
