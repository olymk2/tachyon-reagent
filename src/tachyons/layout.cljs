(ns tachyons.layout)

(defn two-columns [{:keys [column1 column2]}]
  [:article.cf
   [:div.fl.w-50.bg-near-white.tc column1]
   [:div.fl.w-50.bg-light-gray.tc column2]])
